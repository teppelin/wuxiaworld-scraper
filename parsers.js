const cheerio = require("cheerio");
const TurndownService = require("turndown");
const escapes = [
  [/\\/g, "\\\\"],
  [/\*/g, "\\*"],
  [/^-/g, "\\-"],
  [/^\+ /g, "\\+ "],
  [/^(=+)/g, "\\$1"],
  [/^(#{1,6}) /g, "\\$1 "],
  [/`/g, "\\`"],
  [/^~~~/g, "\\~~~"],
  [/\[/g, "\\["],
  [/\]/g, "\\]"],
  [/^>/g, "\\>"],
  [/_/g, "\\_"]
  // [/^(\d+)\. /g, '$1\\. ']
];
const {
  mapChapters,
  normalizeChapters,
  normalizeVolName
} = require("./helpers");

/**
 * TODO: this shit can probably be made into a separate library,
 * since most of this stuff is pretty universal
 */

TurndownService.prototype.escape = (s) =>
  escapes.reduce((acc, esc) => acc.replace(esc[0], esc[1]), s);

const turndownService = new TurndownService();

const list = (html, short) => {
  const $ = cheerio.load(html);

  const xs = $("#accordion")
    .children()
    .map((_, el) => {
      const panel = $(el);

      const volName = panel
        .find("a")
        .eq(0)
        .text()
        .trim();

      const chapters = panel
        .find(".chapter-item")
        .map(mapChapters)
        .toArray();

      return {
        volume: volName,
        chapters
      };
    })
    .toArray()
    .map((x, i) => {
      const bl = [
        "MENDEX (In Progress)",
        "Glossary and Art Gallery",
        "Post Novel",
        "Archfiend: Total Library"
      ];
      if (bl.includes(x.volume)) {
        return;
      }

      return {
        name: normalizeVolName(x.volume, i),
        chapters: x.chapters.map(normalizeChapters, { short })
      };
    })
    .filter(Boolean);

  return xs;
};

const release = (html) => {
  const $ = cheerio.load(html);

  $(".chapter-nav").remove();
  const view = $(".fr-view");
  const viewChildren = view.children();

  if (viewChildren.first().text() === "Previous Chapter") {
    viewChildren.first().remove();
  }

  return turndownService.turndown(view.html());
};

const findChapter = (html, url, short) => {
  const $ = cheerio.load(html);
  const meta = $(".p-15");
  const MediaName = meta
    .find("h4")
    .first()
    .text()
    .trim();
  // release translator
  // const Translator = meta.find(".dl-horizontal > dd").first();
  const a = $(`.chapter-item > a[href="${url.substr(26)}"]`).first();
  const c = a.parent();
  const cidx = c
    .parents()
    .eq(4)
    .find(".chapter-item")
    .index(c);
  const ch = normalizeChapters.bind({ short })(mapChapters(undefined, c), cidx);
  const v = a
    .parents()
    .eq(5)
    .prev(".panel-heading");
  const vidx = v.index(".panel-heading");
  const vn = normalizeVolName(
    v
      .find("a")
      .first()
      .text(),
    vidx
  );

  return {
    MediaName,
    ReleaseGroup: "Wuxiaworld",
    Volume: {
      Name: vn,
      Number: vidx + 1
    },
    Releases: [
      {
        Chapter: {
          AbsoluteNumber: ch.absoluteChapterNumber,
          Number: cidx + 1,
          Name: ch.chapter,
          Slug: ch.chapterSlug,
          Type: ch.type
        },
        URL: url,
        Language: "en"
      }
    ]
  };
};

module.exports = { findChapter, release, list };
