const got = require("got");
const parsers = require("./parsers");
const pLimit = require("p-limit");
const FeedParser = require("feedparser");
const { allNovels } = require("./helpers");

const sendReleases = (msg, exch) =>
  exch.publish(msg, {
    appId: process.env.APP_ID || String(3),
    type: "releases"
  });

/**
 * idk
 * @param {*} ch
 * @returns {Object[]}
 */
const volumeReleases = async ({ params, exch }) => {
  const cs = params.volume.chapters;
  const limit = pLimit(params.concurrency || 3);
  let releases = [];

  for (let i = 0; i < cs.length; i++) {
    const c = cs[i];

    releases.push(
      limit(async () => {
        const r = await got(c.url);
        const content = await parsers.release(r.body);
        return {
          Chapter: {
            AbsoluteNumber: c.absoluteChapterNumber,
            Number: i + 1,
            Name: c.chapter,
            Slug: c.chapterSlug,
            Type: c.type
          },
          URL: c.url,
          Content: content,
          Language: "en"
        };
      })
    );
  }

  releases = await Promise.all(releases);

  const msg = {
    MediaName: params.name,
    ReleaseGroup: "Wuxiaworld",
    Volume: {
      Name: params.volume.name,
      Number: params.volume.volumeNumber
    },
    Releases: releases
  };

  sendReleases(msg, exch);
};

/**
 * gets all chapters of a novel
 * @param {*} param0
 */
const allVolumes = async ({ params, exch }) => {
  const { body: html } = await got(
    `https://www.wuxiaworld.com/novel/${params.slug}`
  );
  const volumes = parsers.list(html, params.short);

  for (let i = 0; i < volumes.length; i++) {
    const volume = { ...volumes[i], volumeNumber: i + 1 };
    await volumeReleases({ params: { ...params, volume }, exch });
  }
};

/**
 * gimme eeeeevverything
 * @param {*} param0
 */
const all = async ({ exch }) => {
  const { body: ns } = await allNovels();

  for (const n of ns.items) {
    await allVolumes({
      params: {
        name: n.name,
        slug: n.slug,
        short: n.abbreviation
      },
      exch
    });
  }
};

const latest = async ({ exch }) => {
  const { body: novels } = await allNovels();
  const feedparser = new FeedParser();

  got.stream("https://www.wuxiaworld.com/feed/chapters").pipe(feedparser);

  feedparser.on("error", (err) => {
    console.error(err);
  });

  feedparser.on("readable", async function() {
    let item;

    while ((item = this.read())) {
      const { link } = item;
      const novel = link
        .split("/")
        .slice(0, -1)
        .join("/");
      const { abbreviation } = novels.items.find(
        (n) => n.name === item.categories[0]
      );
      const { body } = await got(novel);
      const meta = parsers.findChapter(body, link, abbreviation);
      const Content = await got(link).then((r) => parsers.release(r.body));
      const obj = {
        ...meta,
        Releases: meta.Releases.map((r) => ({ ...r, Content }))
      };

      sendReleases(obj, exch);
    }
  });
};

module.exports = { all, latest };
