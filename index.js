const jackrabbit = require("@pager/jackrabbit");
const methods = require("./methods");

const rbt = jackrabbit(process.env.AMQP_URI);

const onMessage = async (data, ack) => {
  ack();

  if (Buffer.isBuffer(data)) {
    data = JSON.parse(data);
  }

  console.log(data);

  // { "method": "all" }
  // { "method": "latest" }

  const exch = rbt.direct();
  exch.queue({ name: "inscribe", durable: true });

  const ctx = { params: data.params, exch };

  try {
    await methods[data.method](ctx);
    console.log(`✔️ Owatta.`);
  } catch (err) {
    console.error(err);
  }
};

rbt
  .default()
  .queue({
    durable: false,
    name: "wuxiaworld_jobs"
  })
  .consume(onMessage);

console.log("✔️ Connected and listening for jobs.");
