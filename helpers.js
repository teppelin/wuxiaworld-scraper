const leven = require("leven");
const _ = require("lodash");
const cheerio = require("cheerio");
const got = require("got");

const VOLUME_REGEX = /(?:book|vol|volume)\s?\w+\s?[-:\u2212\u2013\u2014]\s(?<vol>.+)/i;
const NAME_REGEX = /(?:.*\s?[\d,]+)?(?:[^a-zA-Z]+)(?<name>[^>]+)/i;

const titleTypes = [
  "foreword",
  "preface",
  "prologue",
  "chapter",
  "epilogue",
  "afterword",
  "outro"
];

const normalizeVolName = (str, i) => {
  let name;
  const vm = VOLUME_REGEX.exec(str);

  if (vm === null) {
    switch (str) {
      // make sure this is all of them..
      case "Table of Contents":
      case "Chapters":
        name = `Volume ${i + 1}`;
        break;
      default:
        name = str.trim();
    }
  } else if (!isNaN(str)) {
    name = `Volume ${i + 1}`;
  } else {
    name = vm.groups.vol;
  }

  return name;
};

const normalizeChapters = function(c, i) {
  let chapterName;

  // split the title into words
  const ts = c.title.split(/[^a-zA-Z]/g).filter((t) => /[a-zA-Z]/g.test(t));

  // find the chapter type
  let tt = titleTypes.filter((t) =>
    ts.reduce((a, cur) => {
      return leven(t, cur) <= 2 || (t === "chapter" && cur === this.short)
        ? true
        : a;
    }, false)
  )[0];

  // it's a side story if we didn't match titleTypes :))
  if (tt === undefined) {
    tt = !isNaN(c.title) ? "chapter" : "side story";
  }

  const tm = NAME_REGEX.exec(c.title);
  const plain = `Chapter ${i + 1}`;
  chapterName =
    tt !== "chapter"
      ? _.capitalize(tt)
      : tm === null || !isNaN(tm.groups.name)
      ? plain
      : tm.groups.name.trim() || plain;

  const anRegex = new RegExp(`(?:.*chapter|${this.short})-?(?<an>.*)`, "i");
  const chapterSlug = c.url.split("/")[5];
  const {
    groups: { an: absoluteChapterNumber }
  } = anRegex.exec(chapterSlug);
  return {
    absoluteChapterNumber,
    chapter: chapterName,
    chapterSlug,
    type: tt,
    url: c.url
  };
};

const mapChapters = (_, el) => ({
  title: cheerio(el)
    .text()
    .trim(),
  url:
    "https://www.wuxiaworld.com" +
    cheerio(el)
      .children()
      .first()
      .attr("href")
});

const allNovels = () =>
  got.post("https://www.wuxiaworld.com/api/novels/search", {
    body: {
      title: "",
      tags: [],
      active: null,
      sortType: "Name",
      sortAsc: true,
      searchAfter: null,
      count: 1000
    },
    json: true
  });

module.exports = {
  allNovels,
  mapChapters,
  normalizeChapters,
  normalizeVolName
};
